package main

import (
	"fmt"
	"io/ioutil"
	"os"
	"os/exec"
	"path/filepath"
	"runtime"
	"sort"
	"strconv"
	"strings"
)

type OS int

const (
	Undefined OS = 0
	Windows   OS = 1
	Linux     OS = 2
	Mac       OS = 3
)

func check(e error) {
	if e != nil {
		panic(e)
	}
}

func tryFindPath(file string) string {
	path, err := exec.LookPath(file)
	check(err)
	return path
}

func fileExists(filename string) bool {
	if _, err := os.Stat(filename); os.IsNotExist(err) {
		return false
	}
	return true
}

func getIntVersion(version string) []int {
	intVersion := []int{}
	for _, eachVersion := range strings.Split(version, ".") {
		intEachVersion, err := strconv.Atoi(eachVersion)
		check(err)
		intVersion = append(intVersion, intEachVersion)
	}
	return intVersion
}

func max(x, y int) int {
	if x < y {
		return y
	}
	return x
}

func compareIntVersions(a []int, b []int) int {
	for i := 0; i < max(len(a), len(b)); i++ {
		eachVersionA := 0
		if i < len(a) {
			eachVersionA = a[i]
		}

		eachVersionB := 0
		if i < len(b) {
			eachVersionB = b[i]
		}

		if eachVersionA > eachVersionB {
			return -1
		} else if eachVersionA < eachVersionB {
			return 1
		}
	}

	return 0
}

func pickLatestVersion(versions []string) string {
	maxVersion := versions[0]
	maxIntVersion := getIntVersion(maxVersion)
	for _, version := range versions[1:] {
		intVersion := getIntVersion(version)
		if compareIntVersions(maxIntVersion, intVersion) == 1 {
			maxVersion = version
			maxIntVersion = intVersion
		}
	}

	return maxVersion
}

func querySubDirStrings(path string) []string {
	subDirs, err := ioutil.ReadDir(path)
	check(err)
	subDirStrings := []string{}
	for _, subDir := range subDirs {
		subDirStrings = append(subDirStrings, subDir.Name())
	}

	return subDirStrings
}

func main() {
	arch := "x64"
	var currentOS OS = Undefined

	switch runtime.GOOS {
	case "windows":
		currentOS = Windows
	case "linux":
		currentOS = Linux
	case "darwin":
		currentOS = Mac
	default:
		fmt.Println(runtime.GOOS, "is not supported")
		os.Exit(-1)
	}

	pathTable := make(map[string]string)

	if currentOS == Windows {
		pathTable["CompilerPath"] = tryFindPath("clang-cl.exe")
		pathTable["LinkerPath"] = tryFindPath("lld-link.exe")

		vswhere := exec.Command("./tool/windows/vswhere.exe", "-latest", "-products", "*", "-requires", "Microsoft.VisualStudio.Component.VC.Tools.x86.x64", "-property", "installationPath")
		vswhereOut, err := vswhere.Output()
		check(err)
		pathTable["VSInstallPath"] = strings.TrimSpace(string(vswhereOut))

		vsVersionTxtPath := filepath.Join(pathTable["VSInstallPath"], "VC", "Auxiliary", "Build", "Microsoft.VCToolsVersion.default.txt")
		vsVersionBytes, err := ioutil.ReadFile(vsVersionTxtPath)
		check(err)

		vsVersion := strings.TrimSpace(string(vsVersionBytes))

		vsToolPath := filepath.Join(pathTable["VSInstallPath"], "VC", "Tools", "MSVC", vsVersion)
		pathTable["VSBinPath"] = filepath.Join(vsToolPath, "bin", fmt.Sprintf("Host%s", arch), arch)
		pathTable["VSLibPath"] = filepath.Join(vsToolPath, "lib", arch)
		pathTable["VSIncludePath"] = filepath.Join(vsToolPath, "include")

		pathTable["WindowsSDKBasePath"] = filepath.Join("C:\\", "Program Files (x86)", "Windows Kits", "10")
		winSDKVersions := querySubDirStrings(filepath.Join(pathTable["WindowsSDKBasePath"], "Lib"))
		latestWinSDKVersion := pickLatestVersion(winSDKVersions)
		pathTable["WindowsLibPath"] = filepath.Join(pathTable["WindowsSDKBasePath"], "Lib", latestWinSDKVersion, "um", arch)
		pathTable["WindowsIncludePath"] = filepath.Join(pathTable["WindowsSDKBasePath"], "Include", latestWinSDKVersion, "um")
		pathTable["WindowsUcrtLibPath"] = filepath.Join(pathTable["WindowsSDKBasePath"], "Lib", latestWinSDKVersion, "ucrt", arch)
		pathTable["WindowsUcrtIncludePath"] = filepath.Join(pathTable["WindowsSDKBasePath"], "Include", latestWinSDKVersion, "ucrt")
	} else if currentOS == Linux {
		pathTable["CompilerPath"] = tryFindPath("g++")
		pathTable["LinkerPath"] = tryFindPath("g++")
	}

	keys := []string{}
	for k := range pathTable {
		keys = append(keys, k)
	}
	sort.Strings(keys)

	f, err := os.Create("fastbuild/path.bff")
	check(err)
	defer f.Close()

	for _, key := range keys {
		value := pathTable[key]
		if fileExists(value) {
			str := fmt.Sprintf(".%s = \"%s\"\n", key, value)
			f.WriteString(str)
		}
	}
}
