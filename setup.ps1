function tryBuildGoScript($path)
{
    if (!(Test-Path $path))
    {
        return
    }

    $binPath = [io.path]::ChangeExtension($path, "exe")
    go build -o $binPath $path
}

tryBuildGoScript("./tool/genpathvar/genpathvar.go")