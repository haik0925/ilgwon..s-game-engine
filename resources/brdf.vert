
layout (location = 0) in vec3 a_pos;
layout (location = 1) in vec3 a_normal;
layout (location = 2) in vec2 a_uv;

layout(std140) uniform u_per_frame_decl {
    per_frame_s frame;
};

layout(std140) uniform u_per_object_decl {
    per_object_s object;
};

out vec2 v_uv;
out vec3 v_pos_world;
out vec3 v_normal_world;
out mat3 v_normal_mat;

#if 0
void main()
{
    gl_Position = u_per_frame.proj_mat * u_per_frame.view_mat * vec4(a_pos.xyz, 1);
    v_uv = a_uv;
}
#endif

void main()
{
    mat4 view_model_mat = frame.view_mat * object.model_mat;
    mat3 normal_mat = mat3(transpose(inverse(object.model_mat)));
    v_uv = a_uv;
    v_pos_world = (object.model_mat * vec4(a_pos.xyz, 1)).xyz;
    v_normal_world = normal_mat * a_normal;
    v_normal_mat = normal_mat;
    gl_Position = frame.proj_mat * view_model_mat * vec4(a_pos.xyz, 1);
}