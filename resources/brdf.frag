
in vec2 v_uv;
in vec3 v_pos_world;
in vec3 v_normal_world;
in mat3 v_normal_mat;

out vec4 out_color;

layout(std140) uniform u_per_frame_decl {
    per_frame_s frame;
};

layout(std140) uniform u_per_object_decl {
    per_object_s object;
};

uniform sampler2D albedo_map;
uniform sampler2D ao_map;
uniform sampler2D metallic_map;
uniform sampler2D roughness_map;
uniform sampler2D normal_map;

#if 0
void main()
{             
    //out_color = vec4(1, 0.8, 0.8, 1);
    out_color = texture(diffuse_tex, vec2(v_uv.x, 1 - v_uv.y));
    #if 0
    if(gl_FrontFacing)
        FragColor = texture(frontTexture, TexCoords);
    else
        FragColor = texture(backTexture, TexCoords);
    #endif
}
#endif

#if 1
const float pi = 3.14159265359;

vec3 fresnel_schlick(float cos_theta, vec3 f0)
{
    return f0 + (1.0 - f0) * pow(1.0 - cos_theta, 5.0);
}

float distribution_ggx(vec3 n, vec3 h, float roughness)
{
    float a = roughness * roughness;
    float a_2 = a * a;
    float n_dot_h = max(dot(n, h), 0);
    float n_dot_h_2 = n_dot_h * n_dot_h;

    float num = a_2;
    float denom = (n_dot_h_2 * (a_2 - 1.0) + 1.0);
    denom = pi * denom * denom;

    return num / denom;
}

// TODO: Vectorize roughness
float geometry_schlick_ggx(float n_dot_v, float roughness)
{
    float r = (roughness + 1.0);
    float k = (r * r) / 8.0;

    float num = n_dot_v;
    float denom = n_dot_v * (1.0 - k) + k;

    return num / denom;
}

float geometry_smith(vec3 n, vec3 v, vec3 l, float roughness)
{
    float n_dot_v = max(dot(n, v), 0);
    float n_dot_l = max(dot(n, l), 0);
    float ggx2 = geometry_schlick_ggx(n_dot_v, roughness);
    float ggx1 = geometry_schlick_ggx(n_dot_l, roughness);

    return ggx1 * ggx2;
}

void main()
{
    vec3 n = normalize(v_normal_mat * texture(normal_map, v_uv).xyz);
    //vec3 n = normalize(v_normal_world);
    vec3 v = normalize(frame.cam_pos - v_pos_world);

    vec3 albedo = texture(albedo_map, v_uv).rgb;
    vec3 ao = texture(ao_map, v_uv).rgb;
    //vec3 metallic = texture(metallic_map, v_uv).rgb;
    vec3 metallic = vec3(0);
    vec3 roughness = texture(roughness_map, v_uv).rgb;

    vec3 lo = vec3(0);
    for (int i = 0; i < frame.num_lights; i++)
    {
        vec3 light_position = frame.light_positions[i].xyz;
        vec3 light_color = frame.light_colors[i].rgb;

        //vec3 l = normalize(light_position - v_pos_world);
        vec3 l = normalize(-light_position);
        vec3 h = normalize(v + l);

        float distance = length( - v_pos_world);
        //float attenuation = 1.0 / (distance * distance);
        float attenuation = 10;
        vec3 radiance = light_color * attenuation;

        vec3 f0 = vec3(0.04);
        f0 = mix(f0, albedo, metallic);
        vec3 f = fresnel_schlick(max(dot(h, v), 0), f0);

        float ndf = distribution_ggx(n, h, roughness.r);
        float g = geometry_smith(n, v, l, roughness.r);

        vec3 numerator = ndf * g * f;
        float denominator = 4.0 * max(dot(n, v), 0) * max(dot(n, l), 0);
        vec3 specular = numerator / max(denominator, 0.001);

        vec3 ks = f;
        vec3 kd = vec3(1) - ks;
        kd *= (1.0 - metallic);

        float n_dot_l = max(dot(n, l), 0);
        lo += (kd * albedo / pi + specular) * radiance * n_dot_l;
    }

    vec3 ambient = vec3(0.03) * albedo * ao;
    vec3 color = ambient + lo;

    color = color / (color + vec3(1));
    color = pow(color, vec3(1.0 / 2.2));

    out_color = vec4(color, 1.0);
}
#endif