#include "ige.h"

int appMain(int artc, char **argv) {
  using namespace ige;

  App *app = new App();
  app->init();
  app->run();
  app->cleanup();
  delete app;

  return 0;
}

int main(int argc, char **argv) {
  int ret = appMain(argc, argv);
  return ret;
}