struct VSInput
{
    float3 position : POSITION;
};

struct VSOutput
{
    float4 position : SV_POSITION;
};

cbuffer Frame : register(b0)
{
    float4x4 viewMat;
    float4x4 projMat;
}

VSOutput main(VSInput input)
{
    VSOutput output;
    float4x4 viewProjMat = mul(projMat, viewMat);
    output.position = mul(viewProjMat, float4(input.position.xyz, 1));
    return output;
}