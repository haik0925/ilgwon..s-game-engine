#include "ige.h"
#include <d3d11.h>
#include <d3dcommon.h>
#include <d3dcompiler.h>
#include <stringapiset.h>

namespace ige {

void generateCylinder(float radius, float height, int num_segments,
                      std::vector<Vertex> *out_vertices,
                      std::vector<uint32_t> *out_indices) {
  out_vertices->clear();
  out_vertices->reserve(num_segments * 4 + 2);
  out_indices->clear();
  out_indices->reserve(num_segments * 12);

  float step_radian = 2.f * pi32 / (float)num_segments;
  float half_height = height * 0.5f;

  std::vector<Vertex> top_vertices;
  top_vertices.reserve(num_segments + 1);
  std::vector<uint32_t> top_indices;
  top_indices.reserve(num_segments * 3);
  Float3 top_normal = {0, 1, 0};
  top_vertices.push_back({{0, half_height, 0}, top_normal, {0, 1}});
  for (int i = 0; i < num_segments; ++i) {
    float angle_radian = (float)i * step_radian;
    float c = cosf(angle_radian);
    float s = sinf(angle_radian);
    Float3 pos = {c * radius, half_height, s * radius};
    Float2 uv = {1.f - (angle_radian / (2.f * pi32)),
                 (pos.y + half_height) / height};
    top_vertices.push_back({pos, top_normal, uv});
    top_indices.push_back(0);
    top_indices.push_back(i < (num_segments - 1) ? (i + 2) : 1);
    top_indices.push_back(i + 1);
  }

  for (const Vertex &v : top_vertices) {
    out_vertices->push_back(v);
  }
  for (uint32_t i : top_indices) {
    out_indices->push_back(i);
  }

  int bottom_index_offset = (int)out_vertices->size();
  std::vector<Vertex> bottom_vertices;
  bottom_vertices.reserve(num_segments + 1);
  std::vector<uint32_t> bottom_indices;
  bottom_indices.reserve(num_segments * 3);
  Float3 bottom_normal = {0, -1, 0};
  bottom_vertices.push_back({{0, -half_height, 0}, bottom_normal, {0, 0}});
  for (int i = 0; i < num_segments; ++i) {
    float angle_radian = (float)i * step_radian;
    float c = cosf(angle_radian);
    float s = sinf(angle_radian);
    Float3 pos = {c * radius, -half_height, s * radius};
    Float2 uv = {1.f - (angle_radian / (2.f * pi32)),
                 (pos.y + half_height) / height};
    bottom_vertices.push_back({pos, bottom_normal, uv});
    bottom_indices.push_back(0);
    bottom_indices.push_back(i + 1);
    bottom_indices.push_back(i < (num_segments - 1) ? (i + 2) : 1);
  }

  for (const Vertex &v : bottom_vertices) {
    out_vertices->push_back(v);
  }
  for (uint32_t i : bottom_indices) {
    out_indices->push_back(i + bottom_index_offset);
  }

  int side_index_offset = (int)out_vertices->size();
  std::vector<Vertex> side_vertices;
  side_vertices.reserve(num_segments * 2);
  std::vector<uint32_t> side_indices;
  side_indices.reserve(num_segments * 6);
  for (int i = 0; i < num_segments; ++i) {
    float angle_radian = (float)i * step_radian;
    float x = cosf(angle_radian) * radius;
    float z = sinf(angle_radian) * radius;
    Float3 normal = Float3{x, 0, z}.normalize();
    float u = 1.f - (angle_radian / (2.f * pi32));
    side_vertices.push_back({{x, -half_height, z}, normal, {u, 0}});
    side_vertices.push_back({{x, half_height, z}, normal, {u, 1}});

    int index_base = i * 2;
    bool is_last_segment = i < (num_segments - 1);

    side_indices.push_back(index_base);
    side_indices.push_back(index_base + 1);
    side_indices.push_back(is_last_segment ? (index_base + 3) : 1);

    side_indices.push_back(is_last_segment ? (index_base + 3) : 1);
    side_indices.push_back(is_last_segment ? (index_base + 2) : 0);
    side_indices.push_back(index_base);
  }

  for (const Vertex &v : side_vertices) {
    out_vertices->push_back(v);
  }
  for (uint32_t i : side_indices) {
    out_indices->push_back(i + side_index_offset);
  }
}

void VertexBuffer::init(VertexBufferDesc *desc) {
  ID3D11Device *device = desc->mDevice;

  mVerticesCPU = desc->mVertices;
  mIndicesCPU = desc->mIndices;

  D3D11_BUFFER_DESC verticesDesc = {};
  verticesDesc.BindFlags = D3D11_BIND_VERTEX_BUFFER;
  verticesDesc.ByteWidth = sizeof(Vertex) * mVerticesCPU.size();
  verticesDesc.CPUAccessFlags = 0;
  verticesDesc.Usage = D3D11_USAGE_DEFAULT;
  D3D11_SUBRESOURCE_DATA verticesData = {};
  verticesData.pSysMem = mVerticesCPU.data();

  IGE_COM_ASSERT(
      device->CreateBuffer(&verticesDesc, &verticesData, &mVerticesGPU));

  if (!mIndicesCPU.empty()) {
    D3D11_BUFFER_DESC indicesDesc = {};
    indicesDesc.BindFlags = D3D11_BIND_INDEX_BUFFER;
    indicesDesc.ByteWidth = sizeof(uint32_t) * mIndicesCPU.size();
    indicesDesc.CPUAccessFlags = 0;
    indicesDesc.Usage = D3D11_USAGE_DEFAULT;
    D3D11_SUBRESOURCE_DATA indicesData = {};
    indicesData.pSysMem = mIndicesCPU.data();

    IGE_COM_ASSERT(
        device->CreateBuffer(&indicesDesc, &indicesData, &mIndicesGPU));
  }
}

void Shader::init(ShaderDesc *desc) {
  for (int i = 0; i < (int)ShaderStage::COUNT; ++i) {
    ShaderStage stage = (ShaderStage)i;
    SubShaderDesc &subShaderDesc = desc->mStages[stage];

    if (subShaderDesc.mLoad) {
      LPCSTR target = NULL;
      switch (stage) {
      case ShaderStage::Vertex:
        target = "vs_5_0";
        break;
      case ShaderStage::Pixel:
        target = "ps_5_0";
        break;
      case ShaderStage::Compute:
        target = "cs_5_0";
        break;
      default:
        IGE_ASSERT(false);
      }
      IGE_ASSERT(target);

      ID3DBlob *bytecode = NULL;
      ID3DBlob *error;

      int numWideChars = MultiByteToWideChar(
          CP_UTF8, 0, subShaderDesc.mFilePath.c_str(), -1, NULL, 0);
      wchar_t *filePathWide = new wchar_t[numWideChars];
      MultiByteToWideChar(CP_UTF8, 0, subShaderDesc.mFilePath.c_str(), -1,
                          filePathWide, numWideChars);

      UINT compileFlag =
#ifdef IGE_DEBUG
          D3DCOMPILE_ENABLE_STRICTNESS | D3DCOMPILE_DEBUG |
          D3DCOMPILE_SKIP_OPTIMIZATION;
#else
          D3DCOMPILE_OPTIMIZATION_LEVEL3;
#endif
      HRESULT result =
          D3DCompileFromFile(filePathWide, NULL, NULL, "main", target,
                             compileFlag, 0, &bytecode, &error);
      delete[] filePathWide;

      if (error) {
        IGE_LOG((char *)error->GetBufferPointer());
        safeRelease(error);
      }
      IGE_ASSERT((result == S_OK) && !error);

      switch (stage) {
      case ShaderStage::Vertex:
        mVSBytecode = bytecode;
        mVSBytecode->AddRef();
        desc->mDevice->CreateVertexShader(bytecode->GetBufferPointer(),
                                          bytecode->GetBufferSize(), NULL,
                                          &mVS);
        break;
      case ShaderStage::Pixel:
        desc->mDevice->CreatePixelShader(bytecode->GetBufferPointer(),
                                         bytecode->GetBufferSize(), NULL, &mPS);
        break;
      case ShaderStage::Compute:
        desc->mDevice->CreateComputeShader(bytecode->GetBufferPointer(),
                                           bytecode->GetBufferSize(), NULL,
                                           &mCS);
        break;
      default:
        IGE_ASSERT(false);
      }
      safeRelease(bytecode);
    }
  }
}

void App::init() {
  SDL_Init(SDL_INIT_VIDEO);
  SDL_Window *window =
      SDL_CreateWindow("haha", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED,
                       mWindowWidth, mWindowHeight, 0);

  SDL_SysWMinfo sysinfo = {};
  SDL_VERSION(&sysinfo.version);
  SDL_GetWindowWMInfo(window, &sysinfo);

  DXGI_SWAP_CHAIN_DESC swapChainDesc = {};
  swapChainDesc.BufferCount = 2;
  swapChainDesc.BufferDesc.Width = mWindowWidth;
  swapChainDesc.BufferDesc.Height = mWindowHeight;
  swapChainDesc.BufferDesc.Format = DXGI_FORMAT_R8G8B8A8_UNORM;
  swapChainDesc.BufferDesc.RefreshRate.Numerator = 60;
  swapChainDesc.BufferDesc.RefreshRate.Denominator = 1;
  swapChainDesc.BufferUsage = DXGI_USAGE_RENDER_TARGET_OUTPUT;
  swapChainDesc.OutputWindow = sysinfo.info.win.window;
  swapChainDesc.SampleDesc.Count = 1;
  swapChainDesc.SampleDesc.Quality = 0;
  swapChainDesc.SwapEffect = DXGI_SWAP_EFFECT_FLIP_DISCARD;
  swapChainDesc.Windowed = TRUE;

  D3D_FEATURE_LEVEL desiredFeatureLevels[] = {
      D3D_FEATURE_LEVEL_11_1,
      D3D_FEATURE_LEVEL_11_0,
  };

  UINT createDeviceFlags = D3D11_CREATE_DEVICE_DEBUG;
  IGE_COM_ASSERT(D3D11CreateDeviceAndSwapChain(
      NULL, D3D_DRIVER_TYPE_HARDWARE, NULL, createDeviceFlags,
      desiredFeatureLevels, std::size(desiredFeatureLevels), D3D11_SDK_VERSION,
      &swapChainDesc, &mSwapChain, &mDevice, &mFeatureLevel, &mContext));

  ID3D11Texture2D *backBuffer;
  IGE_COM_ASSERT(mSwapChain->GetBuffer(0, __uuidof(ID3D11Texture2D),
                                       (LPVOID *)&backBuffer));
  IGE_COM_ASSERT(
      mDevice->CreateRenderTargetView(backBuffer, NULL, &mRenderTargetView));
  safeRelease(backBuffer);

  D3D11_TEXTURE2D_DESC depthStencilBufferDesc = {};
  depthStencilBufferDesc.ArraySize = 1;
  depthStencilBufferDesc.BindFlags = D3D11_BIND_DEPTH_STENCIL;
  depthStencilBufferDesc.CPUAccessFlags = 0;
  depthStencilBufferDesc.Format = DXGI_FORMAT_D32_FLOAT;
  depthStencilBufferDesc.Width = mWindowWidth;
  depthStencilBufferDesc.Height = mWindowHeight;
  depthStencilBufferDesc.SampleDesc.Count = 1;
  depthStencilBufferDesc.SampleDesc.Quality = 0;
  IGE_COM_ASSERT(mDevice->CreateTexture2D(&depthStencilBufferDesc, NULL,
                                          &mDepthStencilBuffer));

  IGE_COM_ASSERT(mDevice->CreateDepthStencilView(mDepthStencilBuffer, NULL,
                                                 &mDepthStencilView));

  D3D11_TEXTURE2D_DESC textureDesc = {};
  textureDesc.Width = mWindowWidth;
  textureDesc.Height = mWindowHeight;
  textureDesc.MipLevels = 1;
  textureDesc.ArraySize = 1;
  textureDesc.Format = DXGI_FORMAT_R32G32B32A32_FLOAT;
  textureDesc.SampleDesc.Count = 1;
  textureDesc.Usage = D3D11_USAGE_DEFAULT;
  textureDesc.BindFlags = D3D11_BIND_RENDER_TARGET | D3D11_BIND_SHADER_RESOURCE;
  textureDesc.CPUAccessFlags = 0;
  textureDesc.MiscFlags = 0;
  IGE_COM_ASSERT(
      mDevice->CreateTexture2D(&textureDesc, NULL, &mFramebufferTexture));

  D3D11_RENDER_TARGET_VIEW_DESC renderTargetViewDesc = {};
  renderTargetViewDesc.Format = textureDesc.Format;
  renderTargetViewDesc.ViewDimension = D3D11_RTV_DIMENSION_TEXTURE2D;
  renderTargetViewDesc.Texture2D.MipSlice = 0;
  IGE_COM_ASSERT(mDevice->CreateRenderTargetView(
      mFramebufferTexture, &renderTargetViewDesc, &mFramebufferView));

  D3D11_DEPTH_STENCIL_DESC depthStencilStateDesc = {};
  depthStencilStateDesc.DepthEnable = TRUE;
  depthStencilStateDesc.DepthWriteMask = D3D11_DEPTH_WRITE_MASK_ALL;
  depthStencilStateDesc.DepthFunc = D3D11_COMPARISON_GREATER_EQUAL;
  depthStencilStateDesc.StencilEnable = FALSE;
  IGE_COM_ASSERT(mDevice->CreateDepthStencilState(&depthStencilStateDesc,
                                                  &mDepthStencilState));

  D3D11_RASTERIZER_DESC rasterizerStateDesc = {};
  rasterizerStateDesc.FillMode = D3D11_FILL_WIREFRAME;
  rasterizerStateDesc.CullMode = D3D11_CULL_BACK;
  rasterizerStateDesc.FrontCounterClockwise = TRUE;
  rasterizerStateDesc.DepthClipEnable = TRUE;
  IGE_COM_ASSERT(
      mDevice->CreateRasterizerState(&rasterizerStateDesc, &mRasterizerState));

  char *basePath = SDL_GetBasePath();

  ShaderDesc shaderDesc = {};
  shaderDesc.mDevice = mDevice;
  shaderDesc.mStages[ShaderStage::Vertex].mLoad = true;
  shaderDesc.mStages[ShaderStage::Vertex].mFilePath =
      std::string(basePath) + "..\\ige\\shaders\\basic.vs.hlsl";
  shaderDesc.mStages[ShaderStage::Pixel].mLoad = true;
  shaderDesc.mStages[ShaderStage::Pixel].mFilePath =
      std::string(basePath) + "..\\ige\\shaders\\basic.ps.hlsl";
  mShader.init(&shaderDesc);

  D3D11_INPUT_ELEMENT_DESC inputElementDescs[3] = {};
  inputElementDescs[0].SemanticName = "POSITION";
  inputElementDescs[0].SemanticIndex = 0;
  inputElementDescs[0].Format = DXGI_FORMAT_R32G32B32_FLOAT;
  inputElementDescs[0].InputSlot = 0;
  inputElementDescs[0].AlignedByteOffset = offsetof(Vertex, mPos);
  inputElementDescs[0].InputSlotClass = D3D11_INPUT_PER_VERTEX_DATA;
  inputElementDescs[0].InstanceDataStepRate = 0;
  inputElementDescs[1].SemanticName = "NORMAL";
  inputElementDescs[1].SemanticIndex = 0;
  inputElementDescs[1].Format = DXGI_FORMAT_R32G32B32_FLOAT;
  inputElementDescs[1].InputSlot = 0;
  inputElementDescs[1].AlignedByteOffset = offsetof(Vertex, mNormal);
  inputElementDescs[1].InputSlotClass = D3D11_INPUT_PER_VERTEX_DATA;
  inputElementDescs[1].InstanceDataStepRate = 0;
  inputElementDescs[2].SemanticName = "TEXCOORD";
  inputElementDescs[2].SemanticIndex = 0;
  inputElementDescs[2].Format = DXGI_FORMAT_R32G32_FLOAT;
  inputElementDescs[2].InputSlot = 0;
  inputElementDescs[2].AlignedByteOffset = offsetof(Vertex, mUV);
  inputElementDescs[2].InputSlotClass = D3D11_INPUT_PER_VERTEX_DATA;
  inputElementDescs[2].InstanceDataStepRate = 0;
  IGE_COM_ASSERT(mDevice->CreateInputLayout(
      inputElementDescs, 3, mShader.mVSBytecode->GetBufferPointer(),
      mShader.mVSBytecode->GetBufferSize(), &mInputLayout));

  D3D11_BUFFER_DESC cbDesc = {};
  cbDesc.BindFlags = D3D11_BIND_CONSTANT_BUFFER;
  cbDesc.ByteWidth = sizeof(FrameConstantData);
  cbDesc.CPUAccessFlags = 0;
  cbDesc.Usage = D3D11_USAGE_DEFAULT;
  IGE_COM_ASSERT(mDevice->CreateBuffer(&cbDesc, NULL, &mFrameCb));

  VertexBufferDesc vbDesc = {};
  vbDesc.mDevice = mDevice;
  vbDesc.mVertices = {
      {{-0.5f, -0.5f, 0.f}, {0, 0, 1}, {0, 0}},
      {{0.5f, -0.5f, 0.f}, {0, 0, 1}, {1, 0}},
      {{0, 0.5f, 0.f}, {0, 0, 1}, {0.5f, 1}},
  };
  mTriangleVb.init(&vbDesc);

  vbDesc.mVertices = {
      {{-0.5f, 0, 0.5f}, {0, 1, 0}, {0, 0}},
      {{0.5f, 0, 0.5f}, {0, 1, 0}, {1, 0}},
      {{0.5f, 0, -0.5f}, {0, 1, 0}, {1, 1}},
      {{-0.5f, 0, -0.5f}, {0, 1, 0}, {0, 1}},
  };
  vbDesc.mIndices = {0, 1, 2, 2, 3, 0};
  mPlaneVb.init(&vbDesc);

  generateCylinder(1, 2, 64, &vbDesc.mVertices, &vbDesc.mIndices);
  mSphereVb.init(&vbDesc);

  SDL_free(basePath);
}

void App::run() {
  bool running = true;
  SDL_Event e = {};
  while (running) {
    while (SDL_PollEvent(&e) != 0) {
      if (e.type == SDL_QUIT) {
        running = false;
      }
    }

    update(1.f / 60.f);
    draw();
  }
}

void App::update(float dt) {
  mFrameData.mViewMat = Mat4::lookAt({-1, 1, 1}, {});
  // mFrameData.mViewMat = Mat4::lookAt({0,0.1f, 1}, {});
  float viewHeight = 5.f;
  float viewWidth = viewHeight * (float)mWindowWidth / (float)mWindowHeight;
  mFrameData.mProjMat = Mat4::orthoCenter(viewWidth, viewHeight, 0.1f, 100.f);
}

void App::draw() {
  D3D11_VIEWPORT viewport = {};
  viewport.TopLeftX = 0;
  viewport.TopLeftY = 0;
  viewport.Width = 1280;
  viewport.Height = 720;
  viewport.MinDepth = 0;
  viewport.MaxDepth = 1;
  mContext->RSSetViewports(1, &viewport);

  FLOAT clearColor[4] = {0, 0, 0, 1};
  mContext->ClearRenderTargetView(mRenderTargetView, clearColor);
  mContext->ClearRenderTargetView(mFramebufferView, clearColor);
  mContext->ClearDepthStencilView(mDepthStencilView, D3D11_CLEAR_DEPTH, 0, 0);

  mContext->OMSetRenderTargets(1, &mFramebufferView, mDepthStencilView);
  mContext->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);
  FrameConstantData tempFrameData = {};
  tempFrameData.mViewMat = Mat4::identity();
  tempFrameData.mProjMat = Mat4::identity();
  mContext->UpdateSubresource(mFrameCb, 0, NULL, &tempFrameData, 0, 0);
  mContext->IASetInputLayout(mInputLayout);
  
  mContext->RSSetState(mRasterizerState);
  UINT stride = sizeof(Vertex);
  UINT offset = 0;
  mContext->IASetVertexBuffers(0, 1, &mTriangleVb.mVerticesGPU, &stride,
                               &offset);
  mContext->IASetIndexBuffer(mTriangleVb.mIndicesGPU, DXGI_FORMAT_R32_UINT, 0);
  mContext->VSSetShader(mShader.mVS, NULL, 0);
  mContext->VSSetConstantBuffers(0, 1, &mFrameCb);
  mContext->PSSetShader(mShader.mPS, NULL, 0);

  //mContext->OMSetRenderTargets(1, &mRenderTargetView, mDepthStencilView);
  mContext->OMSetDepthStencilState(mDepthStencilState, 1);
  // mContext->DrawIndexed(mTriangleVb.mIndicesCPU.size(), 0, 0);
  mContext->Draw(mTriangleVb.mVerticesCPU.size(), 0);
  mContext->ClearDepthStencilView(mDepthStencilView, D3D11_CLEAR_DEPTH, 0, 0);

  mContext->UpdateSubresource(mFrameCb, 0, NULL, &mFrameData, 0, 0);
  mContext->OMSetRenderTargets(1, &mRenderTargetView, mDepthStencilView);
  mContext->IASetVertexBuffers(0, 1, &mPlaneVb.mVerticesGPU, &stride, &offset);
  mContext->IASetIndexBuffer(mPlaneVb.mIndicesGPU, DXGI_FORMAT_R32_UINT, 0);
  mContext->DrawIndexed(mPlaneVb.mIndicesCPU.size(), 0, 0);
  mSwapChain->Present(1, 0);
}
} // namespace ige