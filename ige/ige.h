#pragma once
#include "external/SDL2/SDL.h"
#include "external/SDL2/SDL_main.h"
#include "external/SDL2/SDL_syswm.h"
#include "external/fmt/format.h"

#include <d3d11.h>
#include <d3dcommon.h>
#include <vector>
#include <unordered_map>
#include <math.h>

#ifdef IGE_DEBUG
#include <assert.h>
#endif // IGE_DEBUG

#ifdef IGE_DEBUG
#define IGE_ASSERT(exp) assert(exp)
#define IGE_COM_ASSERT(exp)                                                    \
  do {                                                                         \
    HRESULT __hr = (exp);                                                      \
    IGE_ASSERT(!FAILED(__hr));                                                 \
  } while (0)
#define IGE_LOG(...) ige::print(__VA_ARGS__)
#else
#define IGE_ASSERT(exp)
#define IGE_COM_ASSERT(exp) (exp)
#define IGE_LOG(...)
#endif // IGE_DEBUG

namespace ige {

template <typename T> void safeRelease(T *&com) {
  if (com) {
    com->Release();
    com = NULL;
  }
}

template <typename... Args> void print(Args... args) {
  std::string formatted = fmt::format(args...);
  formatted += "\n";
  OutputDebugStringA(formatted.c_str());
}

template <typename E, typename T> struct EnumArray {
  T mElems[(int)(E::COUNT)] = {};

  const T &operator[](E e) const { return mElems[(int)e]; }

  T &operator[](E e) { return mElems[(int)e]; }

  auto begin() { return mElems.begin(); }

  auto end() { return mElems.end(); }

  static_assert(std::is_enum_v<E>);
  static_assert((int64_t)(E::COUNT) > 0);
};

constexpr float pi32 = 3.141592f;

struct Float2 {
  float x = 0.f;
  float y = 0.f;
};

struct Float3 {
  float x = 0.f;
  float y = 0.f;
  float z = 0.f;

  float lengthSq() {
    float result = x * x + y * y + z * z;
    return result;
  }

  float length() {
    float result = sqrtf(lengthSq());
    return result;
  }

  Float3 normalize() {
    float len = length();
    Float3 result = *this / len;
    return result;
  }

  Float3 operator-(const Float3 &other) const {
    Float3 result = {x - other.x, y - other.y, z - other.z};
    return result;
  }

  Float3 operator/(float divider) const {
    Float3 result = {x / divider, y / divider, z / divider};
    return result;
  }
};

inline float dot(const Float3 &a, const Float3 &b) {
  return a.x * b.x + a.y * b.y + a.z * b.z;
}

inline Float3 cross(const Float3 &a, const Float3 &b) {
  Float3 result = {
      a.y * b.z - a.z * b.y,
      a.z * b.x - a.x * b.z,
      a.x * b.y - a.y * b.x,
  };
  return result;
}

struct Float4 {
  float x = 0.f;
  float y = 0.f;
  float z = 0.f;
  float w = 0.f;
};

inline float dot(const Float4 &a, const Float4 &b) {
  return a.x * b.x + a.y * b.y + a.z * b.z + a.w * b.w;
}

struct Mat4 {
  float m[4][4] = {};

  Float4 row(int n) const {
    IGE_ASSERT(n >= 0 && n < 4);
    return {m[0][n], m[1][n], m[2][n], m[3][n]};
  }

  Float4 column(int n) const {
    IGE_ASSERT(n >= 0 && n < 4);
    return {m[n][0], m[n][1], m[n][2], m[n][3]};
  }

  static Mat4 identity() {
    return {{
        {1, 0, 0, 0},
        {0, 1, 0, 0},
        {0, 0, 1, 0},
        {0, 0, 0, 1},
    }};
  }

  static Mat4 lookAt(const Float3 &eye, const Float3 &target,
                     const Float3 &upAxis = {0, 1, 0}) {
    Float3 forward = (eye - target).normalize();
    Float3 right = cross(upAxis, forward).normalize();
    Float3 up = cross(forward, right).normalize();

    // clang-format off
    return {{
      {right.x, up.x, forward.x, 0},
      {right.y, up.y, forward.y, 0},
      {right.z, up.z, forward.z, 0},
      {-dot(eye, right), -dot(eye, up), -dot(eye, forward), 1},
    }};
    // clang-format on
  }

  static Mat4 ortho(float left, float right, float top, float bottom,
                    float nearZ, float farZ) {
#define NDC_MIN_Z 0
#define NDC_MAX_Z 1
    // clang-format off
    Mat4 result = {{
        {2.f / (right - left), 0, 0, 0},
        {0, 2.f / (top - bottom), 0, 0},
        {0, 0, (NDC_MAX_Z - NDC_MIN_Z) / (farZ - nearZ), 0},
        {(left + right) / (left - right), (bottom + top) / (bottom - top),
         farZ * (NDC_MAX_Z - NDC_MIN_Z) / (farZ - nearZ), 1},
    }};
    // clang-format on
    return result;
  }

  static Mat4 orthoCenter(float width, float height, float nearZ, float farZ) {
    float halfWidth = width * 0.5f;
    float halfHeight = height * 0.5f;
    Mat4 result =
        ortho(-halfWidth, halfWidth, halfHeight, -halfHeight, nearZ, farZ);
    return result;
  }
};

inline Mat4 operator*(const Mat4 &a, const Mat4 &b) {
  Float4 rows[4] = {a.row(0), a.row(1), a.row(2), a.row(3)};
  Float4 columns[4] = {b.column(0), b.column(1), b.column(2), b.column(3)};
  Mat4 result;
  for (int i = 0; i < 4; ++i) {
    for (int j = 0; j < 4; ++j) {
      result.m[i][j] = dot(rows[i], columns[j]);
    }
  }
  return result;
}

struct Vertex {
  Float3 mPos;
  Float3 mNormal;
  Float2 mUV;
};

void generateCylinder(float radius, float height, int numSegments,
                      std::vector<Vertex> *outVertices,
                      std::vector<uint32_t> *outIndices);

struct VertexBufferDesc {
  ID3D11Device *mDevice = NULL;
  std::vector<Vertex> mVertices;
  std::vector<uint32_t> mIndices;
};

struct VertexBuffer {
  std::vector<Vertex> mVerticesCPU;
  std::vector<uint32_t> mIndicesCPU;
  ID3D11Buffer *mVerticesGPU = NULL;
  ID3D11Buffer *mIndicesGPU = NULL;

  void init(VertexBufferDesc *desc);
  void cleanup() {
    safeRelease(mVerticesGPU);
    safeRelease(mIndicesGPU);
  }
};

enum class ShaderStage {
  Vertex,
  Pixel,
  Compute,

  COUNT
};

struct SubShaderDesc {
  bool mLoad = false;
  std::string mFilePath;
};

struct ShaderDesc {
  ID3D11Device *mDevice = NULL;
  EnumArray<ShaderStage, SubShaderDesc> mStages;
};

struct Shader {
  ID3DBlob *mVSBytecode = NULL;
  ID3D11VertexShader *mVS = NULL;
  ID3D11PixelShader *mPS = NULL;
  ID3D11ComputeShader *mCS = NULL;

  void init(ShaderDesc *desc);
  void cleanup() {
    safeRelease(mVS);
    safeRelease(mPS);
    safeRelease(mCS);
    safeRelease(mVSBytecode);
  }
};

struct FrameConstantData {
  Mat4 mViewMat;
  Mat4 mProjMat;
};

struct App {
  int mWindowWidth = 1280;
  int mWindowHeight = 720;

  ID3D11Device *mDevice = NULL;
  ID3D11DeviceContext *mContext = NULL;
  IDXGISwapChain *mSwapChain = NULL;
  D3D_FEATURE_LEVEL mFeatureLevel{};
  ID3D11RenderTargetView *mRenderTargetView = NULL;
  ID3D11Texture2D *mDepthStencilBuffer = NULL;
  ID3D11DepthStencilView *mDepthStencilView = NULL;

  ID3D11Texture2D *mFramebufferTexture = NULL;
  ID3D11RenderTargetView *mFramebufferView = NULL;

  ID3D11DepthStencilState *mDepthStencilState = NULL;
  ID3D11RasterizerState *mRasterizerState = NULL;
  Shader mShader;
  ID3D11InputLayout *mInputLayout = NULL;
  ID3D11Buffer *mFrameCb = NULL;
  FrameConstantData mFrameData;

  VertexBuffer mTriangleVb;
  VertexBuffer mPlaneVb;
  VertexBuffer mSphereVb;

  void init();

  void cleanup() {
    mSphereVb.cleanup();
    mPlaneVb.cleanup();
    mTriangleVb.cleanup();

    safeRelease(mFrameCb);
    safeRelease(mInputLayout);
    mShader.cleanup();
    safeRelease(mRasterizerState);
    safeRelease(mDepthStencilState);

    safeRelease(mFramebufferView);
    safeRelease(mFramebufferTexture);

    safeRelease(mDepthStencilView);
    safeRelease(mDepthStencilBuffer);
    safeRelease(mRenderTargetView);
    safeRelease(mSwapChain);
    safeRelease(mContext);
    safeRelease(mDevice);

    SDL_Quit();
  }

  // void onWindowChange();

  void run();
  void update(float dt);
  void draw();
};

} // namespace ige
